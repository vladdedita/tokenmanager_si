#pragma once
#ifndef OBJECTS_H
#include "include/cryptoki.h"
#include "errors.h"
#include <stdio.h>
#include <windows.h>

CK_RV init(char *p11Library) {


	CK_RV					rv;
	HINSTANCE				hDll;
	CK_FUNCTION_LIST_PTR	pFunctionList = NULL;
	CK_C_GetFunctionList	pC_GetFunctionList = NULL;

	//Loading pkcs11 library - TEST - safenet library
	printf("\nLoading pkcs11 library...");
	//pkcs11 library handler
	hDll = LoadLibrary(p11Library);
	if (!hDll)
	{
		printf("ERROR");
		rv = ERR_PKCS11_DLL_NOT_FOUND;
		return rv;
	}
	printf("OK");

	//In order to use the functions from the specified dll
	//it is required to load the functions from the dll 
	// **** DO NOT USE THE FUNCTIONS DEFINED IN cryptoki.h ****

	
	pC_GetFunctionList = (CK_C_GetFunctionList)GetProcAddress(hDll, "C_GetFunctionList"); //this should return a pointer to the C_GetFunctionList
																						 //from the specified dll
	if (pC_GetFunctionList == NULL)
	{
		printf("\nERROR Fetching function list from dll");
		rv = ERR_CRYPTOKIFUNCTION_NOT_FOUND_IN_DLL;
		return rv;
	}

	//pC_GetFunctionList returns a list of the cryptoki functions from the specified dll

	printf("\nLoading PKCS11 functions...");
	rv = (*pC_GetFunctionList)(&pFunctionList);
	if (rv != CKR_OK) {
		printf("ERROR");
		return rv;
	}
	printf("OK");

	printf("\nInitializing PKCS11 library...");
	rv = pFunctionList->C_Initialize(NULL); //passing NULL because the application does not support/use multi threads
	if (!rv == CKR_OK) {
		printf("ERROR");
		rv = ERR_LIBRARY_LOAD;
		return rv;
	}





	

}

#endif