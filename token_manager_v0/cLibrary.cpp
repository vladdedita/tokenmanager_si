#include "cLibrary.h"


CK_RV cLibrary::loadLibrary(char *p11Library) {

	CK_RV rv=CKR_OK;
	//Loading pkcs11 library - TEST - safenet library
	printf("\nLoading pkcs11 library...");
	//pkcs11 library handler
	hDll = LoadLibrary(p11Library);
	if (!hDll)
	{
		printf("ERROR");
		rv = ERR_PKCS11_DLL_NOT_FOUND;

	}
	printf("OK");
	return rv;
}

CK_RV cLibrary::loadDllFunctions()
{
	CK_RV rv = CKR_OK;
	//In order to use the functions from the specified dll
	//it is required to load the functions from the dll 
	// **** DO NOT USE THE FUNCTIONS DEFINED IN cryptoki.h ****

	pC_GetFunctionList = (CK_C_GetFunctionList)GetProcAddress(hDll, "C_GetFunctionList"); //this should return a pointer to the C_GetFunctionList
																						  //from the specified dll
	if (pC_GetFunctionList == NULL)
	{
		printf("\nERROR Fetching function list from dll");
		rv = ERR_CRYPTOKIFUNCTION_NOT_FOUND_IN_DLL;

	}
	return rv;
}

CK_RV cLibrary::getDllFunctions() {
	CK_RV rv = CKR_OK;
	//pC_GetFunctionList returns a list of the cryptoki functions from the specified dll

	printf("\nLoading PKCS11 functions...");
	rv = (*pC_GetFunctionList)(&pFunctionList);
	if (rv != CKR_OK) {
		printf("ERROR");

	}
	printf("OK");
	return rv;
	
}

CK_RV cLibrary::initPKCS11Library() {

	CK_RV rv = CKR_OK;
	printf("\nInitializing PKCS11 library...");
	rv = pFunctionList->C_Initialize(NULL); //passing NULL because the application does not support/use multi threads
	if (!rv == CKR_OK) {
		printf("ERROR");
		rv = ERR_LIBRARY_LOAD;
	}
	printf("OK");
	return rv;
}



