#pragma once
#ifndef SLOT_MGR_H
#define SLOT_MGR_H
#include "include/cryptoki.h"
#include "cLibrary.h"
class cSlotManager {

private:
	

	CK_C_GetSlotList	pC_GetSlotList = NULL_PTR;
	CK_C_GetTokenInfo   pC_GetTokenInfo = NULL_PTR;
	CK_SLOT_ID_PTR		pTokenSlotList=NULL_PTR;
	CK_ULONG			tokenSlotCount;
	CK_BBOOL			tokenPresent;


	CK_RV setTokenSlotList();
	CK_RV checkForSlots();
	CK_RV init() {
		CK_RV rv = CKR_OK;

		while (rv==CKR_OK) {
			setTokenSlotList();
			break;
		}
		return rv;
	}
	void listToken(CK_TOKEN_INFO tokenInfo);
public:
	
	void setGetSlotListFunction(CK_C_GetSlotList f) {
		if(f)
			this->pC_GetSlotList = f;	

		init();
	}
	void setGetTokenInfoFunction(CK_C_GetTokenInfo f) {
		if (f)
			this->pC_GetTokenInfo = f;		
	}

	~cSlotManager() {
		if (pTokenSlotList)
			free(pTokenSlotList);
	}
	void listTokensInfo();


};


#endif