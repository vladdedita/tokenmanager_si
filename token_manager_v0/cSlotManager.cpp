#include "cSlotManager.h"

CK_RV cSlotManager::setTokenSlotList()
{
	CK_RV rv = CKR_OK;
	//There are available slots. 
	//Allocate memory for slot ids ( Slot list )
	pTokenSlotList = (CK_SLOT_ID_PTR)malloc(0);
	tokenSlotCount = 0;


	//Checking for available slots
	checkForSlots();

	printf("\nInitializing token slot list...");
	while (1)
	{
		rv = pC_GetSlotList(CK_TRUE, pTokenSlotList, &tokenSlotCount); // 1st argument let's us search for slots with tokens
	
			if (rv != CKR_BUFFER_TOO_SMALL)
				break;
			pTokenSlotList = (CK_SLOT_ID_PTR)realloc(pTokenSlotList, tokenSlotCount * sizeof(CK_SLOT_ID));
		
	}
		if (!rv == CKR_OK)
		{
			rv = ERR_SLOTS_INIT_LIST;
			printf("ERROR");
			return rv;

		}
		printf("OK\n\tFound %u slot(s) with token", tokenSlotCount);		
	
	return rv;
}

CK_RV cSlotManager::checkForSlots()
{
	CK_RV rv = CKR_OK;

	//Checking for available slots
	CK_ULONG slotCount;
	
	printf("\nChecking for available slots...");
	rv = pC_GetSlotList(CK_FALSE, NULL_PTR, &slotCount);

	
	if (!rv == CKR_OK) {
	
		
		rv = ERR_SLOTS_GET_COUNT;
		printf("ERROR");
		return rv;

	}
	printf("OK\n\tFound %u available slots", slotCount);
	return rv;
}
void cSlotManager::listToken(CK_TOKEN_INFO tokenInfo) {

	printf("\n\tFirmware Version:%d.%d", tokenInfo.firmwareVersion.major, tokenInfo.firmwareVersion.minor);
	printf("\n\tHardware Version:%d.%d", tokenInfo.hardwareVersion.major, tokenInfo.hardwareVersion.minor);
	tokenInfo.label[31] = '\0';
	printf("\n\tLabel:%s", tokenInfo.label);
	tokenInfo.manufacturerID[31] = '\0';
	printf("\n\tManufacturer ID:%s", tokenInfo.manufacturerID);
	tokenInfo.model[15] = '\0';
	printf("\n\tModel:%s",tokenInfo.model);
	tokenInfo.serialNumber[15] = '\0';
	printf("\n\tSerial No.:%s", tokenInfo.serialNumber);
	tokenInfo.utcTime[15] = '\0';
	printf("\n\tUTC Time:%s", tokenInfo.utcTime);	

}
void cSlotManager::listTokensInfo()
{
		CK_RV rv = CKR_OK;
		CK_TOKEN_INFO tokenInfo;
		printf("\n\tListing tokens:");
		for (unsigned int i = 0; i < tokenSlotCount; i++)
		{
			pC_GetTokenInfo(pTokenSlotList[0], &tokenInfo);
			listToken(tokenInfo);
			
		}
		
}
