#pragma once
#ifndef LIBRARY_H
#define LIBRARY_H
#include "include/cryptoki.h"
#include "errors.h"
#include <stdio.h>
#include <windows.h>

class cLibrary {

	
private:
	HINSTANCE				hDll = NULL;
	CK_FUNCTION_LIST_PTR	pFunctionList = NULL;	
	CK_C_GetFunctionList	pC_GetFunctionList = NULL;


private:
	CK_RV init(char *p11Library) {

		CK_RV rv=CKR_OK;
		while (rv == CKR_OK) {
		
			rv = loadLibrary(p11Library);
			rv = loadDllFunctions();
			rv = getDllFunctions();
			rv = initPKCS11Library();
			break;
		
		}
		return rv;
	}	

	CK_RV loadLibrary(char *p11Library);

	CK_RV loadDllFunctions();	

	CK_RV getDllFunctions();

	CK_RV initPKCS11Library();


public:
	
	cLibrary(char *p11Library) {
	
		CK_RV rv=CKR_OK;
		printf("\n\tInitializing library...");
		rv=init(p11Library);
		if (!rv == CKR_OK)
		{
			printf("\n\tInitializing failed");
			delete this;			
		}
		printf("\n\tInitializing succeeded...");
	}

	CK_FUNCTION_LIST_PTR getFunctions() {
		return this->pFunctionList;		
	}



};

#endif



