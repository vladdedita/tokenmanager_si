cLibrary - initialized by the default constructor by <br/>
			passing the dll path 
			***The first thing that should be done*** 
			-usually called by objects accessing resources 
			<br/>

			
			Flow:
			
			cLibrary *lib=new cLibrary("dll path");
			cSlotManager sm=new cSlotManager();
			sm->setGetSlotListFunction(lib->getFunctions()->C_GetSlotList);
			sm->setGetTokenInfoFunction(lib->getFunctions()->C_GetTokenInfo);
			sm->listTokensInfo();




